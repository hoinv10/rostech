GIỚI THIỆU VỀ ROSTECH LAB
=========================
ROSTECH LAB bắt nguồn từ một dự án nghiên cứu phát triển ROBOT ứng dụng tại Việt Nam.

ROSTECH LAB là một trung tâm nghiên cứu công nghệ, được thành lập bởi cộng đồng những người yêu khoa học đến từ Đại học Bách Khoa Hà Nội.
Nhiệm vụ của của ROSTECH LAB là tạo ra một cộng đồng các bạn trẻ đam mê khoa học công nghệ với mục tiêu tạo ra các công ty startup về công nghệ hàng đầu Việt Nam.

Đến với ROSTECH bạn có cơ hội tạo ra các sản phẩm khoa học kỹ thuật với mục tiêu ứng dụng trong thực tế, được chia sẻ những kinh nghiệm giúp thương mại hóa các sản phẩm đó. 
Tham gia vào ROSTECH LAB các bạn sẽ được sống trong một môi trường đông đảo những người đam mê khoa học, có chung mục tiêu, sẵn sàng chia sẻ tất cả các kiến thức trong cuộc sống.
Là thành viên của ROSTECH LAB bạn có động lực để phát triển bản thân, nuôi dưỡng tinh thần startup.

Bạn không cần bỏ bất kỳ chi phí gì khi đến với ROSTECH LAB. Bên cạnh đó, bạn cũng có thể có thêm thu nhập qua những dự án, có cơ hội tìm ra những người bạn có thể bắt tay với nhau cùng tạo nên những điều lớn lao hơn trong cuộc sống.

Chào mừng bạn đến với cộng đồng của chúng tôi!

1. Mô hình tổ chức
------------------

Mô hình dự án được thể hiện chi tiết trong hình sau:
 
.. image:: _img/mohinhduan.PNG
   :width: 300
   :align: center

Nhà đầu tư, hay còn gọi là người hợp tác có nhiệm vụ 
Trong mô hình này, thành viên đặc biệt là phần quan trọng nhất của ROSTECH. Tất cả các thành viên đều có thể trở thành chủ nhiệm dự án